## Запуск
* Клонировать репозиторий
* Установить зависимости, ``pip install -r requirements.txt``
* Запустить скрипт main.py (аргумент "next" для показа следующей недели)
## Пример показа следующей недели
* ``python main.py next``