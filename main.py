"""
Программа была изменена OneEyedDancer <https://codeberg.org/OneEyedDancer/vvsu-schedule>
Основана на работе LinInFal <https://codeberg.org/LinInFal/scrapper-get-sc-vvsu>
"""
import sys
import time
from getpass import getpass

import keyring
from selenium import webdriver
from selenium.common import exceptions
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.edge.service import Service as EdgeService
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.ie.service import Service as IEService
from webdriver_manager import firefox
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from webdriver_manager.microsoft import IEDriverManager

Drivers = [
    lambda: webdriver.Firefox(service=FirefoxService(firefox.GeckoDriverManager().install())),
    lambda: webdriver.Chrome(service=ChromeService(ChromeDriverManager().install())),
    lambda: webdriver.Ie(service=IEService(IEDriverManager().install())),
    lambda: webdriver.Edge(service=EdgeService(EdgeChromiumDriverManager().install())),
]


def get_driver(i=0) -> webdriver.Chrome:
    try:
        return Drivers[i]()
    except exceptions.SessionNotCreatedException as e:
        return get_driver(i + 1)
    except IndexError:
        raise exceptions.SessionNotCreatedException('Необходимый браузер не найден')


class ScheduleParser:
    service_id = "ScheduleParser"

    def __init__(self):
        self.__user_login = None
        self.__user_password = None

    def add_user_data(self):
        print('Чтобы данные для входа не требовалось запрашивать снова, они будут сохранены')
        self.__user_login = input("Введите логин для входа: ")
        self.__user_password = getpass("Пароль: ")
        keyring.set_password(self.service_id, self.__user_login, self.__user_password)

    def get_schedule(self, category='this'):
        user_data = keyring.get_credential(self.service_id, None)
        if user_data is not None:
            self.__user_login = user_data.username
            self.__user_password = user_data.password
        else:
            self.add_user_data()

        browser = get_driver()
        browser.minimize_window()
        browser.get("https://cabinet.vvsu.ru/sign-in")
        browser.find_element(By.NAME, "login").send_keys(self.__user_login)
        time.sleep(0.1)
        browser.find_element(By.NAME, "password").send_keys(self.__user_password)
        time.sleep(0.1)
        browser.find_element(By.NAME, "submit").send_keys(Keys.ENTER)
        time.sleep(1)
        browser.get("https://cabinet.vvsu.ru/time-table/")
        if category == "next":
            try:
                browser.find_element(By.CLASS_NAME, "jcarousel-next").click()
                time.sleep(1)
                container = browser.find_element(By.XPATH, '//*[@id="mycarousel"]')
                content = container.find_element(By.XPATH, '//*[@id="mycarousel"]/li[2]').text
                print(f"\n\n{content}\n\n")
            except Exception as e:
                print(e)
            finally:
                browser.close()
                browser.quit()
        else:
            try:
                sc = browser.find_element(By.CLASS_NAME, "jcarousel-container").text
                print(f"\n\n{sc}\n\n")
            except Exception as e:
                print(e)
            finally:
                browser.close()
                browser.quit()


def main():
    category = None
    if len(sys.argv) > 1:
        category = sys.argv[1]
    ScheduleParser().get_schedule(category)


if __name__ == "__main__":
    main()
